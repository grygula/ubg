/*global $:false */

function gpsSupport() {
    /** Extend Number object with method to convert numeric degrees to radians */
    Number.prototype.toRadians = function () {
        return this * Math.PI / 180;
    };

    /** Extend Number object with method to convert radians to numeric (signed) degrees */
    Number.prototype.toDegrees = function () {
        return this * 180 / Math.PI;
    };
    var targetLongitude = document.getElementById('targetLong').value, targetLatitude = document.getElementById('targetLat').value, compas = document.getElementById("compas"),
        arrowL = $(".arrow-left"), arrowR = $(".arrow-right"), arrowD = $(".arrow-down"), arrowU = $(".arrow-up"), arrowLD = $(".arrow-left-down"), arrowLU = $(".arrow-left-up"),
        arrowRD = $(".arrow-right-down"), arrowRU = $(".arrow-right-up"), distanceField = $("#dstance"),
        answerLatitude = $("#lgt"), answerLongitude = $("#log"), answerAccuracy = $("#acr");

    function setIsDisplay(is, element) {
        if (is) {
            element.removeClass("notActive");
        } else {
            element.addClass("notActive");
        }
    }

    function hiddeALL() {
        setIsDisplay(false, arrowL);
        setIsDisplay(false, arrowR);
        setIsDisplay(false, arrowD);
        setIsDisplay(false, arrowU);
        setIsDisplay(false, arrowLD);
        setIsDisplay(false, arrowLU);
        setIsDisplay(false, arrowRD);
        setIsDisplay(false, arrowRU);
    }

    function setDirectionHelper(isLeft, isRight, isTop, isDown, distance) {
        distanceField.text(distance);
        hiddeALL();
        if (isLeft) {
            if (isTop) {
                setIsDisplay(true, arrowLU);
                return;
            }
            if (isDown) {
                setIsDisplay(true, arrowLD);
                return;
            }
            setIsDisplay(true, arrowL);
            return;
        }
        if (isRight) {
            if (isTop) {
                setIsDisplay(true, arrowRU);
                return;
            }
            if (isDown) {
                setIsDisplay(true, arrowRD);
                return;
            }
            setIsDisplay(true, arrowR);
            return;
        }
        if (isDown) {
            setIsDisplay(true, arrowD);
            return;
        }
        if (isTop) {
            setIsDisplay(true, arrowU);
            return;
        }
    }

    function setDirectionShort(isLeft, isTop, distance) {
        setDirectionHelper(isLeft, !isLeft, isTop, !isTop, distance);
    }


    function getDistance(lat1, lon1, lat2, lon2) {
        var R = 6371000, f1 = lat1.toRadians(), f2 = lat2.toRadians(), df = (lat2 - lat1).toRadians(),
            dl = (lon2 - lon1).toRadians(),
            a = Math.sin(df / 2) * Math.sin(df / 2) + Math.cos(f1) * Math.cos(f2) * Math.sin(dl / 2) * Math.sin(dl / 2),
            c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        return Math.round(R * c);
    }

    function updateDirectionHelper(currentLatitude, currentLongitude) {
        var isTop = targetLatitude > currentLatitude, isLeft = currentLongitude>targetLongitude,
            d = getDistance(currentLatitude, currentLongitude, targetLatitude, targetLongitude);
        setDirectionShort(isLeft, isTop, d + 'm');
        return d;
    }


    function updateCurrentPositionAnswer(pos, distance) {
        answerLatitude.val(pos.coords.latitude);
        answerLongitude.val(pos.coords.longitude);
        answerAccuracy.val(pos.coords.accuracy);
        if (distance < pos.coords.accuracy) {
            $("#sbmt").removeAttr('disabled');
        }
    }

    function displayPositions(pos) {
        var distance = updateDirectionHelper(pos.coords.latitude, pos.coords.longitude);
        updateCurrentPositionAnswer(pos, distance);
    }

    function updateCompas(deg) {
        compas.style.transform = "rotate(" + deg + "deg)";
        compas.style.webkitTransform = "rotate(" + deg + "deg)";
        compas.style.MozTransform = "rotate(" + deg + "deg)";
    }

    function start() {
        targetLongitude = parseFloat(targetLongitude, 10);
        targetLatitude = parseFloat(targetLatitude, 10);
        if (window.navigator.geolocation) {
            navigator.geolocation.watchPosition(displayPositions, function () {
                console.log('gps error');
            }, {enableHighAccuracy: false, timeout: 5000, maximumAge: 0});
        }
        if (window.DeviceOrientationEvent) {
            window.addEventListener('deviceorientation', function (eventData) {
                updateCompas(eventData.alpha);
            }, false);
        }
    }

    start();
}