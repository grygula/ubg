/*global $:false */

function gpsTest() {

    /** Extend Number object with method to convert numeric degrees to radians */
    if (typeof Number.prototype.toRadians == 'undefined') {
        Number.prototype.toRadians = function () {
            return this * Math.PI / 180;
        };
    }

    /** Extend Number object with method to convert radians to numeric (signed) degrees */
    if (typeof Number.prototype.toDegrees == 'undefined') {
        Number.prototype.toDegrees = function () {
            return this * 180 / Math.PI;
        };
    }
    var tm = document.getElementById('time'),
        tfrq = document.getElementById('tfrq'),
        cnter = document.getElementById('count');

    var properties = [
            {'k': 'longitude', 'f': document.getElementById('longitude')},
            {'k': 'latitude', 'f': document.getElementById('latitude')},
            {'k': 'altitude', 'f': document.getElementById('altitude')},
            {'k': 'accuracy', 'f': document.getElementById('accuracy')},
            {'k': 'altitudeAccuracy', 'f': document.getElementById('altitudeAccuracy')},
            {'k': 'heading', 'f': document.getElementById('heading')},
            {'k': 'speed', 'f': document.getElementById('speed')}
        ],
        count = 0, lastMessurment, slctBox = document.getElementById('optionSelect'), gpsWatchID, rsltTxtA, startBtn, stopBtn, trgtLatitude, trgtLongtiude, cmpsv;

    function gameChanged() {
        var v = slctBox[slctBox.selectedIndex].value, trgt = v.split(":");
        rsltTxtA.text("No to ruszamy do: " + v);
        trgtLatitude = parseFloat(trgt[0]);
        trgtLongtiude = parseFloat(trgt[1]);
        startBtn.removeAttr('disabled');
        return false;
    }

    slctBox.onchange = gameChanged;

    function setIsDisplay(is, element) {
        if (is) {
            element.removeClass("notActive");
        } else {
            element.addClass("notActive");
        }
    }

    function hiddeALL() {
        setIsDisplay(false, $(".arrow-left"));
        setIsDisplay(false, $(".arrow-right"));
        setIsDisplay(false, $(".arrow-down"));
        setIsDisplay(false, $(".arrow-up"));
        setIsDisplay(false, $(".arrow-left-down"));
        setIsDisplay(false, $(".arrow-left-up"));
        setIsDisplay(false, $(".arrow-right-down"));
        setIsDisplay(false, $(".arrow-right-up"));
    }

    function setDirectionHelper(isLeft, isRight, isTop, isDown, distance) {
        $("#dstance").text(distance);
        hiddeALL();
        if (isLeft) {
            if (isTop) {
                setIsDisplay(true, $(".arrow-left-up"));
                return;
            }
            if (isDown) {
                setIsDisplay(true, $(".arrow-left-down"));
                return;
            }
            setIsDisplay(true, $(".arrow-left"));
            return;
        }
        if (isRight) {
            if (isTop) {
                setIsDisplay(true, $(".arrow-right-up"));
                return;
            }
            if (isDown) {
                setIsDisplay(true, $(".arrow-right-down"));
                return;
            }
            setIsDisplay(true, $(".arrow-right"));
            return;
        }
        if (isDown) {
            setIsDisplay(true, $(".arrow-down"));
            return;
        }
        if (isTop) {
            setIsDisplay(true, $(".arrow-up"));
            return;
        }
    }

    function getDistance(lat1, lon1, lat2, lon2) {
        var R = 6371000; //m
        var f1 = lat1.toRadians();
        var f2 = lat2.toRadians();
        var df = (lat2 - lat1).toRadians();
        var dl = (lon2 - lon1).toRadians();
        var a = Math.sin(df / 2) * Math.sin(df / 2) +
            Math.cos(f1) * Math.cos(f2) *
            Math.sin(dl / 2) * Math.sin(dl / 2);
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        return Math.round(R * c);
    }

    function setDirectionShort(isLeft, isTop, distance) {
        setDirectionHelper(isLeft, !isLeft, isTop, !isTop, distance);
    }

    function updateHelper(currentLatitude, currentLongitude) {
        var isTop = trgtLatitude > currentLatitude, isLeft = currentLongitude > trgtLongtiude,
            d = getDistance(currentLatitude, currentLongitude, trgtLatitude, trgtLongtiude);
        setDirectionShort(isLeft, isTop, d + 'm');
    }

    function updateWind(pos) {
        updateHelper(pos.coords.latitude, pos.coords.longitude);
    }


    function updateGPSDisplay(pos) {
        var i = properties.length;
        while (i--) {
            properties[i].f.innerHTML = pos.coords[properties[i].k];
        }
        var date = new Date(pos.timestamp), hours = date.getHours(), minutes = date.getMinutes(), seconds = date.getSeconds();
        tm.innerHTML = (hours + ':' + minutes + ':' + seconds);
        tfrq.innerHTML = (pos.timestamp - lastMessurment);
        lastMessurment = pos.timestamp;
        count++;
        cnter.innerHTML = count;
    }

    function displayPositions(pos) {
        updateWind(pos);
        setTimeout(function () {
            updateGPSDisplay(pos);
        }, 2000);
    }

    function onError() {
        console.log('error');
    }

    function readGPS() {
        stopBtn.removeAttr('disabled');
        startBtn.attr('disabled', 'disabled');
        var options = {
            enableHighAccuracy: false,
            timeout: 5000,
            maximumAge: 0
        };

        gpsWatchID = navigator.geolocation.watchPosition(displayPositions, onError, options);
    }

    function stopGPS() {
        navigator.geolocation.clearWatch(gpsWatchID);
        startBtn.removeAttr('disabled');
        stopBtn.attr('disabled', 'disabled');
    }

    var compas = document.getElementById("compas");

    function updateCompas(deg) {
        cmpsv.html(deg);
        compas.style.transform = "rotate(" + deg + "deg)";
        compas.style.webkitTransform = "rotate(" + deg + "deg)";
        compas.style.MozTransform = "rotate(" + deg + "deg)";
    }

    $(document).ready(function () {
        rsltTxtA = $("#txtResult");
        cmpsv = $("#compsV");
        if (window.navigator.geolocation) {
            startBtn = $("#strtGPS");
            stopBtn = $("#stpGPS");

            stopBtn.click(stopGPS);
            startBtn.click(readGPS);

            $("#gpsPresent").removeClass('btn-danger').addClass('btn-success');
            if (window.DeviceOrientationEvent) {
                $("#acsPresent").removeClass('btn-danger').addClass('btn-success');
                window.addEventListener('deviceorientation', function (eventData) {
                    var dir = eventData.alpha;
                    setTimeout(function () {
                        updateCompas(dir);
                    }, 3000);
                }, false);
            }
        } else {
            rsltTxtA.text("GPS nie jest obsługiwany");
        }
    });
}