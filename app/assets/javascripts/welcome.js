/*global $:false */
function wlmcjs() {
    function setFormData(latitude, longitude, acr) {
        $("#lgt").val(latitude);
        $("#log").val(longitude);
        $("#acr").val(acr);
        $("#nick").keyup(function () {
            var nick = this.value;
            if (nick.length > 3) {
                $("#sbmt").removeAttr('disabled');
            } else {
                $("#sbmt").attr('disabled', 'disabled');
            }
        });
    }

    function error(msg) {
        $("#alert").text(msg.message);
    }

    function success(position) {
        $("#gpsInf").hide();
        var crds = position.coords;
        setFormData(crds.latitude, crds.longitude, crds.accuracy);
    }

    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(success, error);
    } else {
        error('not supported');
    }
}