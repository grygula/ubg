class Session < ActiveRecord::Base
	has_many :done_games, dependent: :destroy
end
