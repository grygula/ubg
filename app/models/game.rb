class Game < ActiveRecord::Base
  has_many :done_games, dependent: :destroy
  has_many :tasks, dependent: :destroy
  attr_accessor :distance
end