class DoneGame < ActiveRecord::Base
  belongs_to :game
  belongs_to :session
  has_many :done_tasks, dependent: :destroy, :foreign_key => :donegame_id
end
