class Task < ActiveRecord::Base
  belongs_to :game
  has_many :done_tasks, dependent: :destroy, :foreign_key => :task_id

  def next
    game.tasks.where("id > ?", id).order("id ASC").first
  end
end
