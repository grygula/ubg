json.array!(@sessions) do |session|
  json.extract! session, :id, :browser, :type, :name
  json.url session_url(session, format: :json)
end
