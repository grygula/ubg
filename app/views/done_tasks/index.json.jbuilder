json.array!(@done_tasks) do |done_task|
  json.extract! done_task, :id, :donegame_id, :task_id, :session_id, :result, :endtime
  json.url done_task_url(done_task, format: :json)
end
