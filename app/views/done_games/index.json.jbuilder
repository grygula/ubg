json.array!(@done_games) do |done_game|
  json.extract! done_game, :id, :result, :endtime, :game_id, :session_id
  json.url done_game_url(done_game, format: :json)
end
