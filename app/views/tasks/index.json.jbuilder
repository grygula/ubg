json.array!(@tasks) do |task|
  json.extract! task, :id, :game_id, :title, :content, :tp, :latitude, :longitude, :check, :last
  json.url task_url(task, format: :json)
end
