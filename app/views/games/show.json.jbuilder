json.array!(@games) do |game|
  json.extract! game,:name, :desc,:distance
  json.url task_url(game.tasks.first, format: :json)
end

