class TestController < ApplicationController
  def index
   @games = Game.all
   @ip = request.remote_ip
   @userAgent = request.user_agent
  end
end
