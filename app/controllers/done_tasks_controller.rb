class DoneTasksController < ApplicationController
  before_action :set_done_task, only: [:show, :edit, :update, :destroy]

  # GET /done_tasks
  # GET /done_tasks.json
  def index
    @done_tasks = DoneTask.all
  end

  # GET /done_tasks/1
  # GET /done_tasks/1.json
  def show
  end

  # GET /done_tasks/new
  def new
    @done_task = DoneTask.new
  end

  # GET /done_tasks/1/edit
  def edit
  end

  # POST /done_tasks
  # POST /done_tasks.json
  def create
    @done_task = DoneTask.new(done_task_params)

    respond_to do |format|
      if @done_task.save
        format.html { redirect_to @done_task, notice: 'Done task was successfully created.' }
        format.json { render :show, status: :created, location: @done_task }
      else
        format.html { render :new }
        format.json { render json: @done_task.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /done_tasks/1
  # PATCH/PUT /done_tasks/1.json
  def update
    respond_to do |format|
      if @done_task.update(done_task_params)
        format.html { redirect_to @done_task, notice: 'Done task was successfully updated.' }
        format.json { render :show, status: :ok, location: @done_task }
      else
        format.html { render :edit }
        format.json { render json: @done_task.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /done_tasks/1
  # DELETE /done_tasks/1.json
  def destroy
    @done_task.destroy
    respond_to do |format|
      format.html { redirect_to done_tasks_url, notice: 'Done task was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_done_task
      @done_task = DoneTask.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def done_task_params
      params.require(:done_task).permit(:donegame_id, :task_id, :session_id, :result, :endtime)
    end
end
