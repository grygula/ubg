class DoneGamesController < ApplicationController
  before_action :set_done_game, only: [:show, :edit, :update, :destroy]

  # GET /done_games
  # GET /done_games.json
  def index
    @done_games = DoneGame.all
  end

  # GET /done_games/1
  # GET /done_games/1.json
  def show
  end

  # GET /done_games/new
  def new
    @done_game = DoneGame.new
  end

  # GET /done_games/1/edit
  def edit
  end

  # POST /done_games
  # POST /done_games.json
  def create
    @done_game = DoneGame.new(done_game_params)

    respond_to do |format|
      if @done_game.save
        format.html { redirect_to @done_game, notice: 'Done game was successfully created.' }
        format.json { render :show, status: :created, location: @done_game }
      else
        format.html { render :new }
        format.json { render json: @done_game.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /done_games/1
  # PATCH/PUT /done_games/1.json
  def update
    respond_to do |format|
      if @done_game.update(done_game_params)
        format.html { redirect_to @done_game, notice: 'Done game was successfully updated.' }
        format.json { render :show, status: :ok, location: @done_game }
      else
        format.html { render :edit }
        format.json { render json: @done_game.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /done_games/1
  # DELETE /done_games/1.json
  def destroy
    @done_game.destroy
    respond_to do |format|
      format.html { redirect_to done_games_url, notice: 'Done game was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_done_game
      @done_game = DoneGame.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def done_game_params
      params.require(:done_game).permit(:result, :endtime, :game_id, :session_id)
    end
end
