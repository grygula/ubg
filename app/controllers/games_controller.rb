class GamesController < ApplicationController
  def index
    @games = Game.all
  end

  def end
    @done_game = DoneGame.find(session[:dgm])
    @done_game.result=1
    @done_game.endtime =DateTime.now
    @done_game.save
    @tsks = @done_game.done_tasks
  end

  def show
    # session.delete(:dgm)
    create_session params[:nick]
    @games = Game.all
    currentLongitude = params[:log].to_i
    currentLatitude = params[:lgt].to_i
    @games.each do |g|
      task = g.tasks.first
      distance = getDistance(currentLongitude, currentLatitude, task.longitude, task.latitude)
      g.distance = distance
    end
    @games = @games.to_a
    @games.sort! { |a, b| a.distance <=> b.distance }
  end

  private
  def create_session(nick)
    @session = Session.create(:name => nick.to_s, :browser => request.user_agent.to_s, :ip => request.ip, :tp => 1)
    session[:ses]=@session.id
  end

  def game_params
    params.permit(:lgt, :log, :acr, :nick)
  end

  def getDistance(currentLg, currentLt, targetLg, targetLt)
    r = 6371;
    dLat = (currentLt - targetLt) * Math::PI / 180
    dLon = (currentLg - targetLg)* Math::PI / 180
    a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(targetLt * Math::PI / 180) * Math.cos(currentLt * Math::PI / 180) * Math.sin(dLon / 2) * Math.sin(dLon / 2);
    c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    d = r * c;
    return d;
  end
end
