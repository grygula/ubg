class TasksController < ApplicationController
  before_action :set_task, only: [:show, :answer]

  def show
    case @task.tp
      when 2
        render 'gps'
      when 1
        render 'text'
    end
  end

  def answer
    if @task.tp ==1
      answ = params[:answ]
      if answ == @task.check
        @done_task.result=1
        @done_task.endtime =DateTime.now
        @done_task.points=@task.points
        @done_task.save
        if (@task.last)
          redirect_to gameend_path(@task.game)
        else
          redirect_to @task.next, notice: "Dobrze, a teraz kolejne zadanie"

        end
      else
        redirect_to @task, alert: "Zle, sprobuj ponownie"
      end
    else
      @done_task.result=1
      @done_task.endtime =DateTime.now
      @done_task.points=@task.points
      @done_task.save
      if (@task.last)
        redirect_to gameend_path(@task.game)
      else
        redirect_to @task.next, notice: "Dobrze, a teraz kolejne zadanie"
      end
    end

  end


private
def get_done_task
  @done_task = DoneTask.find(session[:dtsk])
  if (@done_task.task_id!=@task.id)
    @done_task = DoneTask.create(:donegame_id => @done_game.id, :task_id => @task.id, :session_id => @sess.id)
    session[:dtsk]=@done_task.id
  end
rescue ActiveRecord::RecordNotFound
  @done_task = DoneTask.create(:donegame_id => @done_game.id, :task_id => @task.id, :session_id => @sess.id)
  session[:dtsk]=@done_task.id
end

def get_done_game
  @done_game = DoneGame.find(session[:dgm])
rescue ActiveRecord::RecordNotFound
  @done_game = DoneGame.create(:game_id => @task.game.id, :session_id => @sess.id)
  session[:dgm]=@done_game.id
end

def get_session_params

  @sess = Session.find(session[:ses])
  return true
rescue ActiveRecord::RecordNotFound
  redirect_to '', alert: "Musisz sie zalogowac"
  return false
end

def set_task
  @task = Task.find(params[:id])
  r = get_session_params
  get_done_game if r
  get_done_task if r
end

def task_params
  params.require(:task).permit(:game_id, :title, :content, :type, :latitude, :longitude, :check, :last, :answ)
end

end
