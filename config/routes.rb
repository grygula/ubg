Rails.application.routes.draw do
  get 'gps', to: 'test#index', as: 'gps'
  controller :tasks do
    post 'tasks/:id' => :answer
  end
  resources :tasks

  controller :games do
    get 'games' => :index
    post 'games' => :show
    get '/games/:id', to: 'games#end', as: 'gameend'
  end
  resources :done_tasks

  resources :done_games
  resources :sessions
  root 'welcome#index'
end
