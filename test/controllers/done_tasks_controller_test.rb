require 'test_helper'

class DoneTasksControllerTest < ActionController::TestCase
  setup do
    @done_task = done_tasks(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:done_tasks)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create done_task" do
    assert_difference('DoneTask.count') do
      post :create, done_task: { donegame_id: @done_task.donegame_id, endtime: @done_task.endtime, result: @done_task.result, session_id: @done_task.session_id, task_id: @done_task.task_id }
    end

    assert_redirected_to done_task_path(assigns(:done_task))
  end

  test "should show done_task" do
    get :show, id: @done_task
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @done_task
    assert_response :success
  end

  test "should update done_task" do
    patch :update, id: @done_task, done_task: { donegame_id: @done_task.donegame_id, endtime: @done_task.endtime, result: @done_task.result, session_id: @done_task.session_id, task_id: @done_task.task_id }
    assert_redirected_to done_task_path(assigns(:done_task))
  end

  test "should destroy done_task" do
    assert_difference('DoneTask.count', -1) do
      delete :destroy, id: @done_task
    end

    assert_redirected_to done_tasks_path
  end
end
