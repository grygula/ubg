require 'test_helper'

class DoneGamesControllerTest < ActionController::TestCase
  setup do
    @done_game = done_games(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:done_games)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create done_game" do
    assert_difference('DoneGame.count') do
      post :create, done_game: { endtime: @done_game.endtime, game_id: @done_game.game_id, result: @done_game.result, session_id: @done_game.session_id }
    end

    assert_redirected_to done_game_path(assigns(:done_game))
  end

  test "should show done_game" do
    get :show, id: @done_game
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @done_game
    assert_response :success
  end

  test "should update done_game" do
    patch :update, id: @done_game, done_game: { endtime: @done_game.endtime, game_id: @done_game.game_id, result: @done_game.result, session_id: @done_game.session_id }
    assert_redirected_to done_game_path(assigns(:done_game))
  end

  test "should destroy done_game" do
    assert_difference('DoneGame.count', -1) do
      delete :destroy, id: @done_game
    end

    assert_redirected_to done_games_path
  end
end
