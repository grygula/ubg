class CreateDoneTasks < ActiveRecord::Migration
  def change
    create_table :done_tasks do |t|
      t.belongs_to :donegame, index: true
      t.references :task, index: true
      t.references :session, index: true
      t.integer :result
      t.timestamp :endtime

      t.timestamps
    end
  end
end
