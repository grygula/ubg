class CreateTasks < ActiveRecord::Migration
  def change
    create_table :tasks do |t|
      t.belongs_to :game, index: true
      t.string :title
      t.text :content
      t.integer :type
      t.float :latitude
      t.float :longitude
      t.string :check
      t.boolean :last

      t.timestamps
    end
  end
end
