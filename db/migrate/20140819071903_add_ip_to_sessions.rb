class AddIpToSessions < ActiveRecord::Migration
  def change
    add_column :sessions, :ip, :string
    rename_column :sessions, :type, :tp
  end
end
