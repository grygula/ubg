class AddPointsToTasks < ActiveRecord::Migration
  def change
    add_column :tasks, :points, :integer
    add_column :done_tasks, :points, :integer
  end
end
