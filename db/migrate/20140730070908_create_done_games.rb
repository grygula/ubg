class CreateDoneGames < ActiveRecord::Migration
  def change
    create_table :done_games do |t|
      t.integer :result
      t.timestamp :endtime
      t.references :game, index: true
      t.belongs_to :session, index: true

      t.timestamps
    end
  end
end
