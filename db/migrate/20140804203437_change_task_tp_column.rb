class ChangeTaskTpColumn < ActiveRecord::Migration
  def change
    rename_column :tasks, :type, :tp
  end
end
