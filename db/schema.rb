# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140825224251) do

  create_table "done_games", force: true do |t|
    t.integer  "result"
    t.datetime "endtime"
    t.integer  "game_id"
    t.integer  "session_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "done_games", ["game_id"], name: "index_done_games_on_game_id"
  add_index "done_games", ["session_id"], name: "index_done_games_on_session_id"

  create_table "done_tasks", force: true do |t|
    t.integer  "donegame_id"
    t.integer  "task_id"
    t.integer  "session_id"
    t.integer  "result"
    t.datetime "endtime"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "points"
  end

  add_index "done_tasks", ["donegame_id"], name: "index_done_tasks_on_donegame_id"
  add_index "done_tasks", ["session_id"], name: "index_done_tasks_on_session_id"
  add_index "done_tasks", ["task_id"], name: "index_done_tasks_on_task_id"

  create_table "games", force: true do |t|
    t.string   "name"
    t.text     "desc"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "sessions", force: true do |t|
    t.string   "browser"
    t.integer  "tp"
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "ip"
  end

  create_table "tasks", force: true do |t|
    t.integer  "game_id"
    t.string   "title"
    t.text     "content"
    t.integer  "tp"
    t.float    "latitude"
    t.float    "longitude"
    t.string   "check"
    t.boolean  "last"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "points"
  end

  add_index "tasks", ["game_id"], name: "index_tasks_on_game_id"

end
